# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class AmazonSellersItem(scrapy.Item):
    logo = scrapy.Field()
    name = scrapy.Field()
    profile_url = scrapy.Field()
    average_rating = scrapy.Field()
    number_ratings = scrapy.Field()
    phone_number = scrapy.Field()
    rating_change = scrapy.Field()
