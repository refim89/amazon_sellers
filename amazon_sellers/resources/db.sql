CREATE DATABASE sellers;
USE sellers;

CREATE TABLE amazon_sellers(
    logo VARCHAR(256),
    name VARCHAR(128),
    profile_url VARCHAR(256),
    average_rating int,
    number_ratings int,
    phone_number VARCHAR(32),
    rating_change int
);
