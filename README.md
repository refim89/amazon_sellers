1. Install and activate virtual enviroment
    `virtualenv -p python3 .env`
    `source .env/bin/activate`
1. Install requirements1
    `pip install -r requirements.txt`
1. Set mysql database configurations in
    `amazon_sellers/configs.py`
1. Create database and tables using
    `amazon_sellers/resources/db.sql`
1. Run spider
    `scrapy crawl sellers`