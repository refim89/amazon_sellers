import scrapy

from amazon_sellers.configs import TOP_PARSE
from amazon_sellers.items import AmazonSellersItem


class AmazonSpider(scrapy.Spider):
    name = "sellers"
    start_urls = [
        'https://www.marketplacepulse.com/amazon/usa',
    ]

    top_process = TOP_PARSE
    processed = 0

    def parse(self, response):
        if self.processed > self.top_process: return

        amazon_sellers = response.xpath('//*[@id="report"]/tbody/tr//@href').extract()

        left_process = self.top_process - self.processed
        amazon_sellers = amazon_sellers[:max(left_process, 0)]

        for seller in amazon_sellers:
            yield scrapy.Request(
                response.urljoin(seller),
                callback=self.parse_intermediate_info
            )
        self.processed += len(amazon_sellers)

        next_page_begin = '#container > section.row.text-page > div > div > div:nth-child(2) > ' \
                          'div.pagination.border-top > ul > li:nth-child(4) > a ::attr(href)'
        next_page_after_2 = "#container > section.row.text-page > div > div > div:nth-child(2) > " \
                            "div.pagination.border-top > ul > li:nth-child(7) > a ::attr(href)"
        next_page = response.css(next_page_after_2).extract_first()
        if not next_page:
            next_page = response.css(next_page_begin).extract_first()
        yield scrapy.Request(
            response.urljoin(next_page),
            callback=self.parse
        )

    def parse_intermediate_info(self, response):
        amazon_profile = response.css(
            '#container > section.row.text-page.seller-page > div > div > div:nth-child(4) >'
            ' p:nth-child(2) > a ::attr(href)') \
            .extract_first()

        yield scrapy.Request(
            amazon_profile,
            callback=self.parse_profile
        )

    def parse_profile(self, response):
        item = AmazonSellersItem()

        item['logo'] = response.css("#sellerLogo ::attr(src)").extract_first()
        item['name'] = response.css("#sellerName ::text").extract_first()
        item['profile_url'] = response.url

        average_rating, number_ratings = response.css("#seller-feedback-summary > span > a ::text").extract()

        rating_table = response.xpath('//*[@id="feedback-summary-table"]/tr//text()').extract()
        rating_30 = rating_table[7]
        rating_90 = rating_table[11]
        if rating_30 == "-": rating_30 = "0"
        if rating_90 == "-": rating_90 = "0"
        rating_30 = int(rating_30)
        rating_90 = int(rating_90)

        item['phone_number'] = response.css('#seller-contact-phone ::text').extract_first()
        item['number_ratings'] = int(number_ratings.split(" ")[-2].replace("(", ""))
        item['average_rating'] = int(average_rating.split("%")[0])

        item['rating_change'] = rating_30 - rating_90

        yield item
