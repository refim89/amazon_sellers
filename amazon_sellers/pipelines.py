# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import pymysql as pymysql

from amazon_sellers import configs


class AmazonSellersPipeline(object):
    insert_sql = """insert into amazon_sellers (%s) values ( %s )"""

    def __init__(self):
        self.conn = pymysql.connect(host=configs.DB_HOST, unix_socket='/tmp/mysql.sock',
                                    user=configs.DB_USER,
                                    passwd=configs.DB_PASSWORD,
                                    db=configs.DB_NAME,
                                    autocommit = True)

    def __del__(self):
        self.conn.close()

    def process_item(self, item, spider):
        self.insert_data(item, self.insert_sql)

    def insert_data(self, item, insert):
        keys = item.fields.keys()
        fields = ','.join(keys)
        qm = ','.join(['%s'] * len(keys))
        sql = insert % (fields, qm)
        data = [item[k] for k in keys]
        cur = self.conn.cursor()
        cur.execute(sql, data)
        cur.close()

